import './style.scss'

/* IMPORT THREE AND LIBS */
import {gsap} from 'gsap';
import * as THREE from 'three';
import Stats from 'three/examples/jsm/libs/stats.module.js';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial.js';
import { Wireframe } from 'three/examples/jsm/lines/Wireframe.js';
import { LineSegmentsGeometry } from 'three/examples/jsm/lines/LineSegmentsGeometry.js';

import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js'
import { FXAAShader } from 'three/examples/jsm/shaders/FXAAShader.js';

/* --------
  RENDER HELPERS
-------- */
const canvas = document.getElementById('scene');
let wWidth = window.innerWidth;
let wHeight = window.innerHeight;
let windowRatio = wWidth / wHeight;


/* COLORS */
let sceneBg = new THREE.Color(0xE8E8E8);
let groundColor = sceneBg;
let borderColor = new THREE.Color(0xAAAAAA);
let waterColor = new THREE.Color(0xFFFFFF);
let pinColor = new THREE.Color(0x111111);
let pathColor = new THREE.Color(0x82E9A9);

let layerMat, groundMat, borderMat, waterMat, pinMat;


let clock = new THREE.Clock();
let delta = 0;

/* --------
  STATS
-------- */
const statsContainer = document.querySelector('.stats');
let stats = new Stats();
statsContainer.appendChild(stats.dom);


/* --------
  SCENE, CAMERA ETC.
-------- */
const scene = new THREE.Scene();
//scene.fog = new THREE.FogExp2(borderColor, 0.025);
scene.fog = new THREE.Fog(sceneBg, 1, 40);

const camera = new THREE.PerspectiveCamera(75, windowRatio, 0.1, 1000);
const scrollCamera = new THREE.PerspectiveCamera(75, windowRatio, 0.1, 1000);
scrollCamera.position.set(0,84,0);

const renderer = new THREE.WebGL1Renderer({
  canvas: canvas,
  powerPreference: "high-performance",
  antialias: true,
  stencil: false,
  depth: false
});
if ("ontouchstart" in document.documentElement) {
  renderer.setPixelRatio(window.devicePixelRatio);
  //Display in high pixel density on touch
}
renderer.setSize(wWidth,wHeight);

/* POINTER */
const pointer = new THREE.Vector2();

/* COMPOSER */
const composer = new EffectComposer(renderer);
const renderPass = new RenderPass(scene, camera);
composer.addPass(renderPass);

//custom shader pass
const vertShader = document.getElementById('vertexShader').textContent;
const fragShader = document.getElementById('fragmentShader').textContent;
let counter = 0.0;
var myEffect = {
  uniforms: {
    "tDiffuse": { value: null },
    "amount": { value: counter }
  },
  vertexShader: vertShader,
  fragmentShader: fragShader
}

const customPass = new ShaderPass(myEffect);
customPass.renderToScreen = true;
composer.addPass(customPass);

let fxaaPass;
fxaaPass = new ShaderPass( FXAAShader );
fxaaPass.renderToScreen = true;
const pixelRatio = renderer.getPixelRatio();
fxaaPass.material.uniforms[ 'resolution' ].value.x = 1 / ( wWidth * pixelRatio );
fxaaPass.material.uniforms[ 'resolution' ].value.y = 1 / ( wHeight * pixelRatio );

composer.addPass( fxaaPass );


/* HELPERS */
const gridHelper = new THREE.GridHelper( 160, 10 );
//scene.add(gridHelper);

/* --------
  SKYBOX
-------- */
const skyboxGeo = new THREE.SphereGeometry(100,128,24);//TODO: Cylinder
const skyboxMat = new THREE.MeshBasicMaterial({color: sceneBg, side: THREE.BackSide});
const skybox = new THREE.Mesh(skyboxGeo, skyboxMat);
//scene.add(skybox);

/* --------
  WATER
-------- */
const waterGeo = new THREE.PlaneGeometry(200,100);
waterMat = new THREE.MeshBasicMaterial({color: waterColor});
const water = new THREE.Mesh(waterGeo,waterMat);
water.rotation.x = Math.PI / -2;
water.position.set(-10,-0.25,13);
scene.add(water);


/* --------
  POINTS OF INTEREST
-------- */
const points = [
  //{name: "Top", x: 0, y: 0, z: 0, skip: true},
  {name: "Day 01", x: 30.054, y: 0.879, z: 3.771, rotate: -0.5, sprite: "sprites/day01.png", camera: {x:5, y:10, z:5}, highlight: "day01"},
  {name: "Day 05", x: 24.611, y: 2.596, z: -10.711, rotate: 0.1, sprite: "sprites/day05.png", camera: {x:5, y:5, z:1}, highlight: "day05"},
  {name: "Day 07", x: 9.010, y: 4.854, z: -16.285, rotate: 0.25, sprite: "sprites/day07.png", camera: {x:5, y:5, z:5}, highlight: "day07"},
  {name: "Day 09", x: 5.020, y: 3.268, z: -23.305, rotate: 0.05, sprite: "sprites/day09.png", camera: {x:2, y:3, z:-5}, highlight: "day09"},
  {name: "Day 10", x: 2.174, y: 3.444, z: -23.331, rotate: -0.07, sprite: "sprites/day10.png", camera: {x:3, y:6, z:5}, highlight: "day10"},
  {name: "Day 14", x: -7.050, y: 5.592, z: -7.972, rotate: 2.2, sprite: "sprites/day14.png", camera: {x:1, y:4, z:-6}, highlight: "day14"},
  {name: "Day 17", x: -19.879, y: 4.908, z: -7.504, rotate: -0.2, sprite: "sprites/day17.png", camera: {x:3, y:10, z:-5}, highlight: "day17"},
  {name: "Day 19", x: -29.217, y: 5.299, z: -8.227, rotate: -0.75, sprite: "sprites/day19.png", camera: {x:5, y:16, z:8}, highlight: "day19"},
  {name: "Day 30", x: -35.617, y: 1.342, z: 28.848, rotate: -1.25, sprite: "sprites/day30.png", camera: {x:5, y:10, z:5}, highlight: "day30"},
  //{name: "End", x: -36.182, y: 0.811, z: 30.454, rotate: -1.5, skip: true}, 
  {name: "Top", x: 0, y: 0, z: 0, rotate: 0, camera: {x:0, y:72, z:0.75}, highlight: "day30", skip: true, duration: 0.5},
];

/* --------
  TRACKER/
  CAMERA LOCATION
-------- */
const camPosGeo = new THREE.BoxGeometry(1,1,1);
const camPosMat = new THREE.MeshBasicMaterial({color: 0xFFFF00, wireframe: true});
const camPos = new THREE.Mesh(camPosGeo,camPosMat);
pointPosition({elem: camPos, point: points[0], pushX: 2, pushY: 10, pushZ: 5});
camPos.add(camera);

const trackerGeo = new THREE.BoxGeometry(1,1,1);
const trackerMat = new THREE.MeshBasicMaterial({color: 0xFF00FF, wireframe: true});
const tracker = new THREE.Mesh(trackerGeo,trackerMat);
pointPosition({elem: tracker, point: points[0]});

const scrollTracker = new THREE.Vector3(0,0,0);

//Show helpers
//scene.add(camPos);
//scene.add(tracker);

/* --------
  ASSET MANAGER,
  LOAD ASSETS
  & ADD SVG PATHS
-------- */
let loaded = false;
const manager = new THREE.LoadingManager();
const loader = new GLTFLoader(manager);

let blender;
let wireframe;
let matLine;

let borders = new THREE.Group();
let journey;
let journeyY;
let overlay;
layerMat = new THREE.MeshBasicMaterial({color:sceneBg, side: THREE.DoubleSide})
loader.load('/Layers_Big_path_overlay2.glb', function(gltf) {
  console.log(gltf);
  blender = gltf.scene;

  blender.traverse((o) => {
    if (o.isMesh) {
      if(o.name != "Curve121" && o.name != "bg" && o.name != "path" && o.name != "Plane") {
        o.material = layerMat;

        let edgesGeometry = new THREE.EdgesGeometry( o.geometry );
        let lineGeometry = new LineSegmentsGeometry().fromEdgesGeometry( edgesGeometry );
        matLine = new LineMaterial( {
          color: borderColor,
          linewidth: 0.0015,
          fog: true,
          //linewidth: 0.00175 * window.devicePixelRatio,//Note: was 0.001
        } );

        wireframe = new Wireframe( lineGeometry, matLine );
        wireframe.computeLineDistances();
        wireframe.position.copy(o.position);
        wireframe.position.y += 0.00001;
        wireframe.scale.copy(o.scale);
        wireframe.rotation.copy(o.rotation);
        wireframe.name = o.name + '-wireframe';
        borders.add( wireframe );
      }
      else if(o.name == "Curve121") {
        groundMat = new THREE.MeshBasicMaterial({color:groundColor, side: THREE.DoubleSide})
        o.material = groundMat;
      }
      else if(o.name == "path") {
        o.material = new THREE.MeshBasicMaterial({color:pathColor, transparent: true, opacity: 1});
        journey = o;
        journeyY = o.position.y;
      }
      else if(o.name == "Plane") {
        o.material = new THREE.MeshBasicMaterial({color: 0xffffff, transparent: true, opacity: 0, depthWrite: false});
        overlay = o;
      }
      else {
        console.log('skipped ' + o.name);
      }
    }
  });

  console.log(journeyY, overlay);
});
let pin;
let pointsGroup = new THREE.Group();
loader.load('/pin2.glb', function(gltf) {
  gltf.scene.traverse((o) => {
    if(o.isMesh) {
      pin = o;
      pinMat = new THREE.MeshBasicMaterial({color: pinColor, transparent: true});
      o.material = pinMat;
    }
  });
});

const overlayColor = new THREE.TextureLoader(manager).load( 'textures/borders-open.png' );
const overlayAlpha = new THREE.TextureLoader(manager).load( 'textures/borders-open-alpha.png' );


manager.onStart = function(url,itemsLoaded,itemsTotal) {
  //console.log( 'Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.' );
}
manager.onProgress = function ( url, itemsLoaded, itemsTotal ) {
  //console.log( 'Loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.' );
};

/* ASSETS LOADED */
manager.onLoad = function() {
  //console.log('Loading complete');

  /* Show scene */
  loaded = true;
  document.body.classList.add('scene-loaded');

  /* Set scene background? */
  scene.background = sceneBg;

  blender.position.set(-116,0,63.5);
  blender.scale.set(200,200,200);
  borders.position.copy(blender.position);
  borders.scale.copy(blender.scale);
  scene.add(blender);
  scene.add(borders);
  scene.add(pointsGroup);


  points.forEach(function(point,i) {
    let pinGroup = new THREE.Group();
    let name = point.name;
    let x = point.x;
    let y = point.y;
    let z = point.z;

    //Add pin
    let pinMesh = pin.clone();
    pinMesh.position.set(0,0,0);
    pinMesh.rotation.y = point.rotate;
    pinGroup.add(pinMesh);
    
    //Add sprite if set
    if(point.sprite) {
      let spriteOpacity = i == 0 ? 1 : 0;
      let spriteImg = new THREE.TextureLoader().load( point.sprite );
      let spriteMat = new THREE.SpriteMaterial( { map: spriteImg, opacity: spriteOpacity } );
      let sprite = new THREE.Sprite( spriteMat );
      sprite.scale.set(4,2,4);
      sprite.position.set(0,2,-2);
      pinGroup.add(sprite);

      //console.log(sprite);
    }

    //Set position of pin group
    pinGroup.position.set(x,y,z);

    //Add to scene
    if(!point.skip) {
      pointsGroup.add(pinGroup);
    }
  });

  /* Render scene and animate */
  camera.lookAt(tracker.position);

  animate();
}

/* ANIMATE */
function animate() {
  let t = clock.getElapsedTime();
  let s = Math.sin(t * 2.0) * 0.5 + 0.5;

  stats.update();
  
  delta = clock.getDelta();

  camera.lookAt(tracker.position);
  scrollCamera.lookAt(scrollTracker);

  //scrollCamera.position.y -= 0.1;
  //scrollCamera.position.z += 0.05;
  //NOTE: scrollCamera should be positioned in lower y and larget z to get a good position

  composer.render();

  /* SET ACTIVE LOCATION */
  let currentPoint = points[activePoint];
  pointPosition({elem: tracker, point: currentPoint, duration: currentPoint.duration, lerp: true});
  pointPosition({elem: camPos, point: currentPoint, duration: currentPoint.duration, pushX: currentPoint.camera.x, pushY: currentPoint.camera.y, pushZ: currentPoint.camera.z, lerp: true});

  if(currentPoint.name == "Top") {
    gsap.to(scene.fog, {far:300, duration: 10});

    borders.children.forEach(function(border) {
      gsap.to(border.material, {linewidth: 0.001});
    });
  }
  else {
    gsap.to(scene.fog, {far: 40, duration: 10});
    camera.position.lerp(new THREE.Vector3(0,pointer.y * 0.75,pointer.x * -0.7),0.05);
    renderPass.camera = camera;
    borders.children.forEach(function(border) {
      gsap.to(border.material, {linewidth: 0.0015});
    });
  }

  //Move camera based on mouse
  //camera.position.lerp(new THREE.Vector3(0,pointer.y * 0.75,pointer.x * -0.7),0.05);

  counter += 0.01;
  customPass.uniforms["amount"].value = counter;

  requestAnimationFrame(animate);
}


/* RESIZE */
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  scrollCamera.aspect = window.innerWidth / window.innerHeight;
  scrollCamera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth,window.innerHeight);
  composer.setSize(window.innerWidth,window.innerHeight);

  const pixelRatio = renderer.getPixelRatio();
  fxaaPass.material.uniforms[ 'resolution' ].value.x = 1 / ( window.innerWidth * pixelRatio );
  fxaaPass.material.uniforms[ 'resolution' ].value.y = 1 / ( window.innerHeight * pixelRatio );
}
window.addEventListener( 'resize', onWindowResize );


if ("ontouchstart" in document.documentElement) {
  function onDocumentTouchEnd(event) {
    event.preventDefault();
  
    pointer.x = 0;
    pointer.y = 0;
  }

  //console.log('is touch');
}
else {
  function onMouseMove( event ) {
    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components
  
    pointer.x = ( event.pageX / window.innerWidth ) * 2 - 1;
    pointer.y = - ( event.pageY / window.innerHeight ) * 2 + 1;
  }
  window.addEventListener( 'pointermove', onMouseMove);
  //console.log('is NOT touch');
}

function pointPosition(params) {
  let lerp = params.lerp ? true : false;
  let lerpAlpha = params.lerpAlpha ? params.lerpAlpha : 0.03;
  let gsapDuration = params.duration ? params.duration : 3;
  let gsapDelay = params.delay ? params.delay : 0;
  let pushX = params.pushX ? params.pushX : 0;
  let pushY = params.pushY ? params.pushY : 0;
  let pushZ = params.pushZ ? params.pushZ : 0;
  let posX = params.point.x + pushX;
  let posY = params.point.y + pushY;
  let posZ = params.point.z + pushZ;

  if(lerp) {
    gsap.to(params.elem.position, {duration: gsapDuration, delay: gsapDelay, x: posX, y: posY, z: posZ, ease: "power2"});
    //params.elem.position.lerp(new THREE.Vector3(x,y,z),lerpAlpha);
  }
  else {
    params.elem.position.set(posX,posY,posZ);
  }
}

let activePoint = 0;
let totalPoints = points.length - 1;
console.log(totalPoints);
function setActive() {
  const prev = document.querySelector('.gui-nav__prev');
  const next = document.querySelector('.gui-nav__next');

  next.addEventListener('click', function() {
    if(activePoint < totalPoints) {
      activePoint += 1;
    }
    else {
      activePoint = 0;
    }

    //console.log(activePoint);
    showSprite();
    activeHighlight();
  });
  prev.addEventListener('click', function() {
    if(activePoint > 0) {
      activePoint -= 1;
    }
    else {
      activePoint = totalPoints;
    }

    //console.log(activePoint);
    showSprite();
    activeHighlight();
  });
}
setActive();

function showSprite() {
  pointsGroup.children.forEach(function(child,i) {
    if(i == activePoint) {
      child.children.forEach(function(inner) {
        if(inner.type == "Sprite") {
          gsap.to(inner.material, {opacity: 1, duration: 1});
        }
      });
    }
    else {
      child.children.forEach(function(inner) {
        if(inner.type == "Sprite") {
          gsap.to(inner.material, {opacity: 0, duration: 0.5});
        }
      });
    }
  });
}

function activeHighlight() {
  let currentActive =  document.querySelector('.highlight--active');
  if(currentActive) {
    currentActive.classList.remove('highlight--active');
  }

  let highlight = document.querySelector('[data-highlight="' + points[activePoint].highlight + '"');
  console.log(highlight);
  highlight.classList.add('highlight--active');
}

const readmoreButton = document.querySelector('.gui-nav__readmore');
function openHighlight() {
  const highlightsContainer = document.querySelector('.highlights');
  
  highlightsContainer.classList.toggle('highlights--open');
  activeHighlight();
}
readmoreButton.addEventListener('click', openHighlight);
